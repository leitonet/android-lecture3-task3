package com.example.l3task3;

import android.content.Intent;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity {

    Button go;
    int i = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        go = (Button) findViewById(R.id.go);
        go.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                i++;
                Handler handler = new Handler();

                handler.postDelayed(new Runnable(){

                    @Override
                    public void run() {
                        if(i == 1){
                            Intent intent = new Intent(MainActivity.this, Main2Activity.class);
                            startActivity(intent);
                        }else if(i == 2){
                            Intent intent = new Intent(MainActivity.this, Main3Activity.class);
                            startActivity(intent);
                        }else if (i == 3){
                            Intent intent = new Intent(MainActivity.this, Main4Activity.class);
                            startActivity(intent);
                        }
                        i = 0;
                    }
                }, 500);
            }
        });

    }

    @Override
    protected void onResume() {
        super.onResume();
    }


}
